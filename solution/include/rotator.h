#ifndef ROTATOR_H
#define ROTATOR_H

/* create image copy, that rotated by 90 degrees counter-clockwise */
struct image image_rotate(struct image const source);

#endif
